## The Lab Front End

Home of the Lab Front end web application.

## Getting Started

### Dependencies
* Yarn
* Node

### Setup

Starting from the freshly cloned repo:

1. `yarn`
2. `yarn run start`

## Documentation

See the [docs](./docs/README.md) section.

## Credit

This project is based off [react-boilerplate](https://github.com/mxstbr/react-boilerplate) by Maximilian
Stoiber. Check the repo for more information, updates and fixes.
