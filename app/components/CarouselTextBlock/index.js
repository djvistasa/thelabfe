import React from 'react';
import PropTypes from 'prop-types';
import Button from '../Button';
import './styles.scss';

function CarouselTextBlock(props) {
  const { blockContent } = props;
  return (
    <div className="carousel-text-block">
      <div className="block-inner">
        <h1>{blockContent.title}</h1>
        <p>{blockContent.description}</p>
        <Button title="LINK TEXT" />
      </div>
    </div>
  );
}

CarouselTextBlock.propTypes = {
  blockContent: PropTypes.object,
};

export default CarouselTextBlock;
