import React from 'react';
import PropTypes from 'prop-types';
import { Carousel } from 'react-responsive-carousel';
import 'react-responsive-carousel/lib/styles/carousel.min.css';
import CarouselTextBlock from '../CarouselTextBlock';
import './styles.scss';

function Slider(props) {
  const { blockContent } = props;
  return (
    <div className="carousel">
      <Carousel
        showThumbs={false}
        showStatus={false}
        showArrows={false}
        autoPlay
        infiniteLoop
        useKeyboardArrows
        stopOnHover
      >
        <div className="slide slide-one"> </div>
        <div className="slide slide-two"> </div>
        <div className="slide slide-three"> </div>
      </Carousel>
      <CarouselTextBlock blockContent={blockContent} />
    </div>
  );
}

Slider.propTypes = {
  blockContent: PropTypes.object,
};

export default Slider;
