import React from 'react';
import './styles.scss';

function ContactEmail() {
  return (
    <div className="contact-email">
      <div className="email">
        <a
          className="footer-mailto-link"
          href="mailto:hello@lab.co.uk?subject=Hello%20Lab"
        >
          hello@lab.co.uk
        </a>
      </div>
    </div>
  );
}

export default ContactEmail;
