import React from 'react';
import PropTypes from 'prop-types';
import SVGInline from 'react-svg-inline';
import prevIcon from '../../images/icons/prev.svg';
import nextIcon from '../../images/icons/next.svg';
import './styles.scss';

class HappinessSlider extends React.PureComponent { // eslint-disable-line
  // react/prefer-stateless-function
  constructor(props) {
    super(props);

    this.state = {
      happinessLevel: 0,
    };
  }

  static propTypes = {
    getHappinessValue: PropTypes.func,
  };

  getHappinessSlides = () => [
    {
      id: 0,
      value: 0,
    },
    {
      id: 1,
      value: 1,
    },
    {
      id: 2,
      value: 2,
    },
    {
      id: 3,
      value: 3,
    },
    {
      id: 4,
      value: 4,
    },
  ];

  toggleHappiness = value => {
    const { getHappinessValue } = this.props;
    if (value === 'next') {
      if (this.state.happinessLevel < this.getHappinessSlides().length - 1) {
        this.setState({ happinessLevel: this.state.happinessLevel + 1 }, () => {
          getHappinessValue(this.state.happinessLevel);
        });
      }
    } else if (value === 'prev') {
      if (this.state.happinessLevel > 0) {
        this.setState({ happinessLevel: this.state.happinessLevel - 1 }, () => {
          getHappinessValue(this.state.happinessLevel);
        });
      }
    }
  };

  render() {
    const { happinessLevel } = this.state;
    return (
      <div className="happiness-slider">
        <div
          className="slide-wrapper"
          style={{
            width: `${this.getHappinessSlides().length * 100}%`,
            left: `-${happinessLevel * 100}%`,
          }}
        >
          {this.getHappinessSlides().map(slide => (
            <div
              key={slide.id}
              className={`${happinessLevel === slide.id ? 'active' : 'inactive'}
              slide slide-${slide.id + 1}`}
            />
          ))}
        </div>
        <div className="slide-controls">
          <div
            role="button"
            tabIndex={0}
            onClick={() => this.toggleHappiness('prev')}
            onKeyDown={() => this.toggleHappiness('')}
            className="prev control"
          >
            <SVGInline svg={prevIcon} />
          </div>
          <div
            role="button"
            tabIndex={0}
            onClick={() => this.toggleHappiness('next')}
            onKeyDown={() => this.toggleHappiness('next')}
            className="next control"
          >
            <SVGInline svg={nextIcon} />
          </div>
        </div>
        <div className="happiness-indicator-track">
          <div
            className="level"
            style={{ left: `${(100 / 4) * happinessLevel}%` }}
          />
        </div>
      </div>
    );
  }
}

export default HappinessSlider;
