import React from 'react';
import PropTypes from 'prop-types';
import WorkBlock from '../WorkBlock';
import './styles.scss';

function WhatWeDo(props) {
  const { workTypeData } = props;
  return (
    <div className="what-we-do">
      {workTypeData.map(type => <WorkBlock key={type.id} type={type} />)}
      <div className="clear-fix"> </div>
    </div>
  );
}

WhatWeDo.propTypes = {
  workTypeData: PropTypes.array,
};

export default WhatWeDo;
