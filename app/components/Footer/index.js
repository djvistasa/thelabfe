import React from 'react';
import PropTypes from 'prop-types';
import SocialIcons from '../SocialIcons';
import AwardsSlider from '../AwardsSlider';
import ContactEmail from '../ContactEmail';
import CopyrightInfo from '../CopyrightInfo';
import TelNumber from '../TelNumber';
import './styles.scss';

function Footer(props) {
  const { awards } = props;
  return (
    <div className="footer">
      <div className="tablet ">
        <SocialIcons />
        <ContactEmail />
        <TelNumber />
      </div>
      <AwardsSlider awards={awards} />
      <CopyrightInfo />
    </div>
  );
}

Footer.propTypes = {
  awards: PropTypes.array,
};

export default Footer;
