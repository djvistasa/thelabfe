import React from 'react';
import findIndex from 'lodash/findIndex';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import Select from '@material-ui/core/Select';
import FormControl from '@material-ui/core/FormControl';
import MenuItem from '@material-ui/core/MenuItem';

import './styles.scss';

function SelectControl(props) {
  const {
    input,
    options,
    disabled,
    placeholder = 'select one',
    meta: { touched, error, warning },
  } = props;

  const stateClass = touched && ((error && 'error') || (warning && 'warning'));
  const fieldClasses = classNames('select-field', {
    warning: touched && warning,
    error: touched && error,
    valid: input.value !== '',
    disabled,
  });
  const selectedIndex = findIndex(options, { value: input.value });

  return (
    <div className={`form-select ${fieldClasses}`}>
      <FormControl>
        <Select
          {...input}
          className={classNames(stateClass, 'select-input', {
            placeholder: selectedIndex < 0,
          })}
          displayEmpty
          disabled={disabled}
          onBlur={undefined}
        >
          <MenuItem value="">{placeholder}</MenuItem>
          {options.map(option => (
            <MenuItem key={option.id} value={option.value}>
              {option.value}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
      {(touched && (error && <span className="error-message">{error}</span>)) ||
        (warning && <span className="warning-message">{warning}</span>)}
    </div>
  );
}

SelectControl.propTypes = {
  input: PropTypes.object,
  options: PropTypes.array,
  placeholder: PropTypes.string,
  meta: PropTypes.object,
  disabled: PropTypes.bool,
};

export default SelectControl;
