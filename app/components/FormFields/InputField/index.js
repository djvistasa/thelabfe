import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import Input from '@material-ui/core/Input';

import './styles.scss';

function InputField(props) {
  const {
    input,
    label,
    type,
    placeholder,
    disabled = false,
    autoComplete = 'on',
    isSignUp,
    isSearchField,
    meta: { touched, error, warning },
  } = props;

  const fieldClasses = classNames('text-field', {
    'search-field': isSearchField,
    warning: touched && warning,
    error: touched && error,
    valid: !error && !warning,
    disabled,
    hidden: type === 'hidden',
  });

  const passwordStrength = inputChars => {
    const { length } = inputChars;
    let className = '';
    if (length >= 0 && length <= 2) {
      className = 'weak';
    } else if (length >= 2 && length <= 7) {
      className = 'average';
    } else if (length >= 7) {
      className = 'good';
    }
    return className;
  };

  let strengthClass = '';
  if (type === 'password' && isSignUp) {
    strengthClass = passwordStrength(input.value);
  }

  const passwordStrengthMessage = input.value ? (
    <span className={`password-strength ${strengthClass}`}>
      Strength: {strengthClass}{' '}
    </span>
  ) : (
    <span className="password-message">Strength: At Least 8 Characters</span>
  );
  const element = document.getElementById(input.name);

  if (element && element !== null) {
    document.getElementById(input.name).addEventListener('keydown', el => {
      if (el.which === 38 || el.which === 40) {
        el.preventDefault();
      }
    });
  }

  return (
    <div
      className={`form-text-field ${fieldClasses}`}
      id={error ? input.name : ''}
    >
      <label htmlFor={input.name}>
        <span className="content">
          <span>{label}</span>
        </span>
        <Input
          {...input}
          type={type}
          placeholder={placeholder}
          label={placeholder}
          autoComplete={autoComplete || 'off'}
          disabled={disabled}
          className="floating-text-input"
          id={input.type === 'number' ? input.name : null}
        />
      </label>
      {type === 'password' && isSignUp && !error && passwordStrengthMessage}
      {touched &&
        ((error && <span className="error-message">{error}</span>) ||
          (warning && <span className="warning-message">{warning}</span>))}
    </div>
  );
}

InputField.propTypes = {
  input: PropTypes.object,
  label: PropTypes.string,
  type: PropTypes.string,
  placeholder: PropTypes.string,
  autoComplete: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
  disabled: PropTypes.bool,
  isSignUp: PropTypes.bool,
  isSearchField: PropTypes.bool,
  meta: PropTypes.object,
};

export default InputField;
