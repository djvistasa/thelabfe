import React from 'react';
import PropTypes from 'prop-types';
import './styles.scss';

function WorkBlock(props) {
  const { type } = props;
  return (
    <div
      className="work-block"
      style={{ backgroundImage: `url(${type.image})` }}
    >
      <p>{type.title}</p>
    </div>
  );
}

WorkBlock.propTypes = {
  type: PropTypes.object,
};

export default WorkBlock;
