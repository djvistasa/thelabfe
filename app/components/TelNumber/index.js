import React from 'react';
import './styles.scss';

function TelNumber() {
  return (
    <div className="tel-number">
      <div className="tel">
        <a href="tel:0207 183 6668">0207 183 6668</a>
      </div>
    </div>
  );
}

export default TelNumber;
