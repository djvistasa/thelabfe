import React from 'react';
import { reduxForm, Field, change } from 'redux-form/immutable';
import PropTypes from 'prop-types';
import InputField from '../../FormFields/InputField';
import Select from '../../FormFields/SelectControl';
import Button from '../../Button';
import { emailValidation, required, phoneNumber } from '../validation';
import './styles.scss';

export class ContactForm extends React.PureComponent {
  static propTypes = {
    handleSubmit: PropTypes.func,
    submitting: PropTypes.bool,
    happinessLevel: PropTypes.number,
    dispatch: PropTypes.func,
    form: PropTypes.string,
  };

  componentWillReceiveProps(np) {
    const { form, dispatch } = this.props;

    if (np.happinessLevel !== this.props.happinessLevel) {
      dispatch(change(form, 'happinessLevel', np.happinessLevel));
    }
  }

  render() {
    const { handleSubmit, submitting, happinessLevel } = this.props;
    return (
      <form onSubmit={handleSubmit} className="contact-form">
        <h1>Elegant Communication</h1>
        <Field
          name="fullName"
          component={InputField}
          type="text"
          placeholder="full name"
        />
        <Field
          name="emailAddress"
          component={InputField}
          type="text"
          placeholder="email address*"
          validate={[required, emailValidation]}
        />
        <Field
          name="contactNumber"
          component={InputField}
          type="text"
          placeholder="contact number"
          validate={phoneNumber}
        />
        <Field
          name="happinessLevel"
          component={InputField}
          type="hidden"
          value={happinessLevel}
        />
        <Field
          name="digitalStrategy"
          component={Select}
          type="text"
          placeholder="digital strategy of transformation"
          options={[{ value: 'Option 1', id: 0 }, { value: 'Option 2', id: 1 }]}
        />
        <Button
          title="Contact Me"
          onClick={handleSubmit}
          disabled={submitting}
        />
      </form>
    );
  }
}

export default reduxForm({
  form: 'contactForm', // a unique identifier for this form
})(ContactForm);
