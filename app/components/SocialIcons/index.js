import React from 'react';
import SVGInline from 'react-svg-inline';
import FacebookIcon from '../../images/icons/facebook.svg';
import TwitterIcon from '../../images/icons/twitter.svg';
import LinkedInIcon from '../../images/icons/linkedin.svg';
import './styles.scss';

function SocialIcons() {
  return (
    <div className="social-icons">
      <a
        href="https://www.linkedin.com/company/lab-digital-agency"
        target="_blank"
        className="linkedIn"
      >
        <SVGInline svg={LinkedInIcon} />
      </a>
      <a
        href="https://twitter.com/LabDigitalUK"
        target="_blank"
        className="facebook"
      >
        <SVGInline svg={FacebookIcon} />
      </a>
      <a
        href="https://www.facebook.com/LabDigitalAgency"
        target="_blank"
        className="twitter"
      >
        <SVGInline svg={TwitterIcon} />
      </a>
    </div>
  );
}

export default SocialIcons;
