import React from 'react';
import PropTypes from 'prop-types';
import HappinessSlider from '../HappinessSlider';
import './styles.scss';

function HappinessIndicator(props) {
  const { getHappinessLevel } = props;
  const getHappinessValue = value => getHappinessLevel(value); //eslint-disable-line
  return (
    <div className="happiness-indicator">
      <div className="title-section">
        <h1>Happiness Indicator</h1>
        <p>How happy are you right now?</p>
      </div>
      <HappinessSlider getHappinessValue={getHappinessValue} />
    </div>
  );
}

HappinessIndicator.propTypes = {
  getHappinessLevel: PropTypes.func,
};

export default HappinessIndicator;
