import React from 'react';
import './styles.scss';

function CopyrightInfo() {
  const year = new Date().getFullYear();
  return (
    <div className="copyright-info">
      © {year} Copyright Lab Lateral. All Rights Reserved |&nbsp;
      <a href="/privacy-and-cookies-policy">Privacy &amp; Cookies Policy</a>
    </div>
  );
}

export default CopyrightInfo;
