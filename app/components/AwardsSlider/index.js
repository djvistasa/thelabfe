import React from 'react';
import PropTypes from 'prop-types';
import SVGInline from 'react-svg-inline';
import prevIcon from '../../images/icons/prev.svg';
import nextIcon from '../../images/icons/next.svg';
import './styles.scss';

class AwardsSlider extends React.PureComponent { // eslint-disable-line
  // react/prefer-stateless-function
  static propTypes = {
    awards: PropTypes.array,
  };

  constructor(props) {
    super(props);
    this.state = {
      activeSlide: 0,
      windowWidth: window.innerWidth,
      resizeFunc: this.updateWindowWidth.bind(this),
    };
  }

  componentDidMount() {
    window.addEventListener('resize', this.state.resizeFunc);
  }
  componentWillUnmount() {
    window.removeEventListener('resize', this.state.resizeFunc);
  }

  updateWindowWidth() {
    this.setState({ windowWidth: window.innerWidth, activeSlide: 0 });
  }

  toggleActiveSlide = value => {
    const { activeSlide, windowWidth } = this.state;
    const { awards } = this.props;
    if (value === 'next') {
      if (
        activeSlide <
        (windowWidth >= 768 ? awards.length / 3 : awards.length) - 1
      ) {
        this.setState({ activeSlide: this.state.activeSlide + 1 });
      }
    } else if (value === 'prev') {
      if (activeSlide > 0) {
        this.setState({ activeSlide: this.state.activeSlide - 1 });
      }
    }
  };
  render() {
    const { awards } = this.props;
    const { activeSlide } = this.state;
    return (
      <div className="awards-wrapper">
        <div
          className="awards-inner"
          style={{
            width: `${awards.length * 100}%`,
            left: `-${activeSlide * 100}%`,
          }}
        >
          {awards.map(award => (
            <div key={award.id} className="award">
              <h1 className="title">{award.title}</h1>
              <h1 className="sub-title">{award.subtitle}</h1>
              <h1 className="year">{award.year}</h1>
            </div>
          ))}
        </div>
        <div className="awards-controls">
          <div
            role="button"
            tabIndex={0}
            onClick={() => this.toggleActiveSlide('prev')}
            onKeyDown={() => this.toggleActiveSlide('prev')}
            className="prev control"
          >
            <SVGInline svg={prevIcon} />
          </div>
          <div
            role="button"
            tabIndex={0}
            onClick={() => this.toggleActiveSlide('next')}
            onKeyDown={() => this.toggleActiveSlide('next')}
            className="next control"
          >
            <SVGInline svg={nextIcon} />
          </div>
        </div>
      </div>
    );
  }
}

export default AwardsSlider;
