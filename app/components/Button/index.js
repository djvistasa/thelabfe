/*
 * Props
 * classNames = List of classNames you would like to use on button, space delimited
 */

import React from 'react';
import PropTypes from 'prop-types';
import './styles.scss';

function Button(props) {
  const { title, disabled, onClick, classNames, type } = props;
  const handleClick = () => {
    if (typeof onClick === 'function' && onClick) {
      onClick();
    }
  };
  return (
    <button
      className={`button ${classNames}`}
      type={type}
      onClick={handleClick}
      disabled={disabled}
    >
      {title}
    </button>
  );
}

Button.defaultProps = {
  classNames: '',
  disabled: false,
};

Button.propTypes = {
  onClick: PropTypes.func,
  disabled: PropTypes.bool,
  title: PropTypes.string,
  classNames: PropTypes.string,
  type: PropTypes.string,
};

export default Button;
