import React from 'react';
import ClassNames from 'classnames';
import { Link } from 'react-router-dom';
import './styles.scss';

class Navigation extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      isMenuOpen: false,
    };
  }

  toggleMenu = () => this.setState({ isMenuOpen: !this.state.isMenuOpen });

  getNavItems = () => {
    const navItems = [
      {
        title: 'Our Work',
        id: 0,
        link: '/',
      },
      {
        title: 'Secret Sauce',
        id: 1,
        link: '/',
      },
      {
        title: 'Insight',
        id: 2,
        link: '/',
      },
      {
        title: 'Careers',
        id: 3,
        link: '/',
      },
    ];

    return navItems.map(item => (
      <li key={item.id}>
        <Link
          onClick={() => {
            this.toggleMenu();
          }}
          to={{ pathname: item.link }}
        >
          {item.title}
        </Link>
      </li>
    ));
  };

  render() {
    const { isMenuOpen } = this.state;
    const triggerClasses = ClassNames({ open: isMenuOpen });
    const menuClasses = ClassNames({ open: isMenuOpen, closed: !isMenuOpen });
    return (
      <nav className="navigation-wrapper">
        <div className="header-wrapper">
          <div className="logo-wrapper">
            <Link to={{ pathname: '/' }}> </Link>
          </div>
          <div
            role="button"
            tabIndex={0}
            onClick={() => this.toggleMenu()}
            onKeyDown={() => this.toggleMenu()}
            className={`${triggerClasses} trigger mobile`}
          >
            <span />
            <span />
            <span />
          </div>
        </div>
        <ul className={`${menuClasses} menu mobile`}>{this.getNavItems()}</ul>
        <ul className={`${menuClasses} desktop desktop-nav`}>
          {this.getNavItems()}
        </ul>
        <div className="clear-fix"> </div>
      </nav>
    );
  }
}

export default Navigation;
