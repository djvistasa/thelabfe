/**
 *
 * App.js
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import { Switch, Route } from 'react-router-dom';

import Home from 'containers/Home/Loadable';
import NotFoundPage from 'containers/NotFoundPage/Loadable';
import Navigation from '../../components/Navigation';
import Footer from '../../components/Footer';

export default function App() {
  const setAwards = () => [
    {
      id: 0,
      title: '+ rar digital awards',
      subtitle: 'grand prix',
      year: 'winner 2016',
    },
    {
      id: 1,
      title: '+ rar digital awards',
      subtitle: 'grand prix',
      year: 'winner 2017',
    },
    {
      id: 2,
      title: '+ rar digital awards',
      subtitle: 'grand prix',
      year: 'winner 2018',
    },
    {
      id: 3,
      title: '+ rar digital awards',
      subtitle: 'grand prix',
      year: 'winner 2016',
    },
    {
      id: 4,
      title: '+ rar digital awards',
      subtitle: 'grand prix',
      year: 'winner 2016',
    },
    {
      id: 5,
      title: '+ rar digital awards',
      subtitle: 'grand prix',
      year: 'winner 2016',
    },
  ];
  return (
    <div>
      <Navigation />
      <Switch>
        <Route exact path="/" component={Home} />
        <Route component={NotFoundPage} />
      </Switch>
      <Footer awards={setAwards()} />
    </div>
  );
}
