import React from 'react';
// import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { compose } from 'redux';
import Slider from '../../components/Slider';
import WhatWeDo from '../../components/WhatWeDo';
import HappinessIndicator from '../../components/HappinessIndicator';
import ContactForm from '../../components/Forms/ContactForm';
import { workTypeData } from './workTypeData';
import './styles.scss';

export class Home extends React.Component { // eslint-disable-line
  // react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {
      happinessLevel: 0,
    };
  }
  handleSubmit = value => console.log('FORM VALUES', value); // eslint-disable-line

  render() {
    const { happinessLevel } = this.state;
    const initialValues = {
      happinessLevel,
    };
    const carouselBlockContent = {
      title: 'Elegant Communication',
      description:
        "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
    };
    return (
      <div>
        <Helmet>
          <title>Home</title>
          <meta name="description" content="Description of Home" />
        </Helmet>
        <div className="home page-wrapper">
          <Slider blockContent={carouselBlockContent} />
          <WhatWeDo workTypeData={workTypeData} />
          <div className="contact-wrapper">
            <div className="contact-section">
              <HappinessIndicator
                getHappinessLevel={happiness => {
                  this.setState({ happinessLevel: happiness });
                }}
              />
              <ContactForm
                onSubmit={this.handleSubmit}
                initialValues={initialValues}
                happinessLevel={happinessLevel}
              />
              <div className="clear-fix"> </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Home.propTypes = {};

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  null,
  mapDispatchToProps,
);

export default compose(withConnect)(Home);
