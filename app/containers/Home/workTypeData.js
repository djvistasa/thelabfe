import analyst from '../../images/work/analyst.jpg';
import design from '../../images/work/design.jpg';
import development from '../../images/work/development.jpg';
import digitalStrategy from '../../images/work/digital-strategy.jpg';
import neuroLinguist from '../../images/work/neuro-linguist.jpg';
import neuroMarketing from '../..//images/work/neuromarketing.jpg';
import performanceMarketing from '../../images/work/performance-marketing.jpg';
import video from '../../images/work/video.jpg';

export const workTypeData = [
  {
    id: 0,
    title: 'Video',
    image: video,
  },
  {
    id: 1,
    title: 'Analyst',
    image: analyst,
  },
  {
    id: 2,
    title: 'Design',
    image: design,
  },
  {
    id: 3,
    title: 'Development',
    image: development,
  },
  {
    id: 4,
    title: 'Digital Strategy',
    image: digitalStrategy,
  },
  {
    id: 5,
    title: 'Neuro Linguist',
    image: neuroLinguist,
  },
  {
    id: 6,
    title: 'Neuro Marketing',
    image: neuroMarketing,
  },
  {
    id: 7,
    title: 'Performance Marketing',
    image: performanceMarketing,
  },
];
